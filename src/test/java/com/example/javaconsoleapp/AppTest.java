package com.example.javaconsoleapp;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.*;

/**
 * <p>App unit tests.</p>
 *
 * <p>Test method naming convention: productionMethod_stateUnderTest_expectedBehavior.</p>
 *
 * <p>
 *     These methods must be safe to execute in parallel.  For example, tests involving the capture of stdout would
 *     break under parallel execution.
 * </p>
 */
@Tag("parallel")
class AppTest
{
    private static final Logger LOGGER = LogManager.getLogger(
            AppTest.class
    );

    /**
     * <p>method: {@link App#processArg(String)}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void processArg_normal_normal()
    {
        LOGGER.trace("begin processArg_normal_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        String actual = App.processArg(
                "Tom"
        );

        LOGGER.debug(
                "actual: ",
                actual
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertEquals(
                "Hello, Tom!",
                actual,
                "Should return expected String."
        );

        // just sleeping here to illustrate difference between serial and
        // parallel tests
        try
        {
            Thread.sleep(3000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        LOGGER.trace("end processArg_normal_normal()");
    }

    /**
     * <p>method: {@link App#processArgs(String[],PrintStream)}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void processArgs_normal_normal()
    {
        LOGGER.trace("begin processArgs_normal_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        String[] args = {
                "Tom"
        };

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        App.processArgs(
                args,
                new PrintStream(
                        byteArrayOutputStream
                )
        );

        String string = byteArrayOutputStream.toString();

        LOGGER.debug(
                "string: {}",
                string
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertEquals(
                "Hello, Tom!\n",
                string,
                "Printed string should match."
        );

        // just sleeping here to illustrate difference between serial and
        // parallel tests
        try
        {
            Thread.sleep(3000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        LOGGER.trace("end processArgs_normal_normal()");
    }
}