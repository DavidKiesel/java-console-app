package com.example.javaconsoleapp;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.*;

/**
 * <p>App unit tests.</p>
 *
 * <p>Test method naming convention: productionMethod_stateUnderTest_expectedBehavior.</p>
 *
 * <p>
 *     These methods must be executed serially because they would be unsafe to execute in parallel.  For example, tests
 *     involving the capture of stdout would break under parallel execution.
 * </p>
 */
@Tag("serial")
class AppSerialTest
{
    private static final Logger LOGGER = LogManager.getLogger(
            AppSerialTest.class
    );

    private PrintStream originalSystemOut;
    private ByteArrayOutputStream systemOutContent;

    @BeforeEach
    void redirectSystemOutStream()
    {
        originalSystemOut = System.out;

        systemOutContent = new ByteArrayOutputStream();

        System.setOut(
                new PrintStream(systemOutContent)
        );
    }

    @AfterEach
    void restoreSystemOutStream()
    {
        System.setOut(
                originalSystemOut
        );
    }

    /**
     * <p>method: {@link App#main(String[])}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void main_normal_normal()
    {
        LOGGER.trace("begin main_normal_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        String[] args = {
                "Tom"
        };

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        App.main(args);

        String string = systemOutContent.toString();

        LOGGER.debug(
                "string: {}",
                string
        );

        LOGGER.debug(
                "string.indexOf(\"\\n\"): {}",
                string.indexOf("\n")
        );

        String[] strings = string.split(
                "\n"
        );

        int stringNum = 0;

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertEquals(
                "Hello, Tom!",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "Hello, world!",
                strings[stringNum],
                "System.out should match."
        );

        Assertions.assertEquals(
                2,
                strings.length,
                "Number of lines in System.out should match."
        );

        // just sleeping here to illustrate difference between serial and
        // parallel tests
        try
        {
            Thread.sleep(3000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        LOGGER.trace("end main_normal_normal()");
    }

    /**
     * <p>method: {@link App#main(String[])}</p>
     *
     * <p>state: no args</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void main_noArgs_normal()
    {
        LOGGER.trace("begin main_noArgs_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        String[] args = new String[0];

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        App.main(args);

        String string = systemOutContent.toString();

        LOGGER.debug(
                "string: {}",
                string
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertEquals(
                "Hello, world!\n",
                string,
                "System.out should match."
        );

        // just sleeping here to illustrate difference between serial and
        // parallel tests
        try
        {
            Thread.sleep(3000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        LOGGER.trace("end main_noArgs_normal()");
    }
}