package com.example.javaconsoleapp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * <p>App integration tests.</p>
 *
 * <p>Test method naming convention: productionMethod_stateUnderTest_expectedBehavior.</p>
 *
 * <p>These could be integration tests that require connecting to some external service.</p>
 */
@Tag("integration")
class AppIT
{
    private static final Logger LOGGER = LogManager.getLogger(
            AppIT.class
    );

    /**
     * <p>method: {@link App#main(String[])}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void main_normal_normal()
    {
        LOGGER.trace("begin main_normal_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertTrue(
                true,
                "Always true."
        );

        LOGGER.trace("end main_normal_normal()");
    }
}