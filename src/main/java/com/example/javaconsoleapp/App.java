package com.example.javaconsoleapp;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.PrintStream;

/**
 * <p>Java application for testing.</p>
 */
public class App
{
    //////////////////////////////////////////////////////////////////////////
    // class static fields

    private static final Logger LOGGER = LogManager.getLogger(
            App.class
    );

    //////////////////////////////////////////////////////////////////////////
    // class static methods

    /**
     * <p>Main entry point.</p>
     *
     * @param args  program arguments
     */
    public static void main(
            String[] args
    )
    {
        LOGGER.trace("begin main(String[])");

        processArgs(
                args,
                System.out
        );

        System.out.println(
                "Hello, world!"
        );

        LOGGER.trace("end main()");
    }

    /**
     * <p>Process arguments.</p>
     *
     * @param args  A {@link String} array.
     */
    @SuppressWarnings("WeakerAccess")
    public static void processArgs(
            String[] args,
            PrintStream printStream
    )
    {
        LOGGER.trace("begin processArgs(String[],PrintStream)");

        for (String arg : args)
        {
            printStream.println(
                    processArg(arg)
            );
        }

        LOGGER.trace("end processArgs(String[],PrintStream)");
    }

    /**
     * <p>Process an argument.</p>
     *
     * @param arg  A {@link String}.
     * @return  "Hello, arg!"
     */
    @SuppressWarnings("WeakerAccess")
    public static String processArg(
            String arg
    )
    {
        LOGGER.trace("begin processArg(String)");

        String string = "Hello, " + arg + "!";

        LOGGER.debug(
                "string: {}",
                string
        );

        LOGGER.trace("end processArg(String)");

        return string;
    }
}
